       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRINGLE-1.
       AUTHOR. SUPHAKORN
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SCR-LINE  PIC X(80) VALUE SPACES.
       01 STAR-NUM  PIC 9(3)  VALUE ZEROS.  
           88 VALID-STAR-NUM VALUE 1 THRU 80 .   
       01 INDEX-NUM PIC 9(3) VALUE ZEROS.
       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 002-INPUT-STAR  THRU 002-EXIT 
           PERFORM 001-PRINT-LINE THRU 001-EXIT 
              VARYING INDEX-NUM FROM 1 BY 1 
              UNTIL INDEX-NUM > STAR-NUM 
           GOBACK 
       .
       001-PRINT-LINE.
           MOVE ALL "*" TO SCR-LINE(1:INDEX-NUM)
           DISPLAY SCR-LINE  
           .    

       001-EXIT.
           EXIT
           .

       002-INPUT-STAR.
           PERFORM UNTIL VALID-STAR-NUM
                   DISPLAY "PLEASE INPUT STAR NUMBER: " WITH NO
                      ADVANCING 
                   ACCEPT STAR-NUM
                   IF NOT VALID-STAR-NUM 
                       DISPLAY "PLEASE INPUT NUMBER POSITIVE 0 "
           END-PERFORM

           .    

       002-EXIT.
           EXIT 
       .
       