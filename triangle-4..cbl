       IDENTIFICATION DIVISION. 
       PROGRAM-ID. TRINGLE-3.
       AUTHOR. SUPHAKORN
       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01 SCR-LINE  PIC X(80) VALUE SPACES.
       01 STAR-NUM  PIC 9(3)  VALUE ZEROS.  
           88 VALID-STAR-NUM VALUE 1 THRU 80 .   
       01 INDEX-NUM PIC 9(3) VALUE ZEROS.
       01 INDEX-NUM2 PIC 9(3) VALUE ZEROS .
       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-INPUT-STAR  THRU 001-EXIT 
           PERFORM 002-STAR-LINE THRU 002-EXIT
              VARYING INDEX-NUM FROM 5 BY -1
              UNTIL INDEX-NUM = 0  
           GOBACK 
       .
      
       001-INPUT-STAR.
           PERFORM UNTIL VALID-STAR-NUM
                   DISPLAY "PLEASE INPUT STAR NUMBER: " WITH NO
                      ADVANCING 
                   ACCEPT STAR-NUM
                   IF NOT VALID-STAR-NUM 
                       DISPLAY "PLEASE INPUT NUMBER POSITIVE 0 "
           END-PERFORM

           .    

       001-EXIT.
           EXIT 
       .

       002-STAR-LINE.
           COMPUTE INDEX-NUM2 = STAR-NUM - INDEX-NUM + 1
           MOVE ALL SPACES TO SCR-LINE
           MOVE ALL "*" TO SCR-LINE(INDEX-NUM2: INDEX-NUM)
           DISPLAY SCR-LINE 
       .

       002-EXIT.
           EXIT 
       .
       